\begin{lemma}
\label{lemma:lifting-from-bdy}
Let $\Omega\subset\BbbR^2$ be a polygon. Then there exists a bounded linear
$G: H^{1/2}_{pw}(\partial\Omega) \rightarrow H^2(\Omega)$ with
$(\partial_n (G g) - \bi k G g)|_{\partial\Omega} = g$
and
\begin{eqnarray*}
\|G g\|_{1,k} &\leq& C k^{-1/2} \|g\|_{L^2(\partial\Omega)} \\
\|G g\|_{H^2(\Omega)} &\leq& C \|g\|_{H^{1/2}_{pw}(\partial\Omega)} + k^{1/2} \|g\|_{L^2(\partial\Omega)}. 
\end{eqnarray*}
\end{lemma}
\begin{proof}
In the interest of brevity, we base the proof on the solvability theory in
convex polygons. \newline
\emph{step~1:} Let $T$ be a (convex) triangle. Then one can infer (see
\cite[Lemma~{4.6}]{melenk-sauter09} for the details) from standard
arguments using the Lax-Milgram Lemma and $H^2$-regularity for elliptic problems
on convex polygons (see, e.g., \cite[Cor.~{4.4.3.8}]{grisvard85a}) the existence
of $C_{T}>0$ such that for every $ g\in H_{pw}^{1/2}(\partial T)$ there holds for
the solutions $u^{\pm} \in H^{2}(T)$ of
\begin{equation*}
-\Delta u + k^2 u=0\quad \mbox{ in $T$},\qquad \partial _{n}u \pm  \bi k u =g\quad 
\mbox{ on $\partial T$}
\end{equation*}
the \textsl{a priori} bound $\Vert u^\pm\Vert _{H^{2}(T)}\leq C_{T}\Vert g\Vert
_{H_{pw}^{1/2}(\partial T)} + k^{1/2} \|g\|_{L^2(\partial T)}$ together
with $\|u^\pm \|_{1,k} \leq k^{-1/2} \|g\|_{L^2(\partial T)}$.

\emph{step~2:} Let $S=\{(r\cos \varphi \,|\,r\sin \varphi )\,|\,0<r<2\delta
,0<\varphi <\omega \}$ with edges $\Gamma _{1}$, $\Gamma _{2}$ meeting at
the origin. Set $\Gamma _{1,\delta }:=\{(r,0)\,|\,0<r<\delta \}$, $\Gamma
_{2,\delta }:=\{(r\cos \omega ,r\sin \omega )\,|\,0<r<\delta \}$.
For the case of a convex sector, i.e., $0 < \omega < \pi$, it is easy to
construct with the aid of the first step a bounded linear operators $L^\pm:
\prod_{i=1}^2 \{u \in H^{1/2}(\Gamma_i)\,|\, \operatorname*{supp} u \subset 
\overline{\Gamma_{i,\delta}}\} \rightarrow H^2(S)$ with
$(\partial_n L^\pm (f_1,f_2))|_{\Gamma_i} \pm \bi k L^\pm (f_1,f_2) = f_i$ ($i \in \{1,2\}$)
and $\|L^\pm  (f_1,f_2)\|_{H^2(S)}
\leq C \sum_{i=1}^2 \left[ \|f_i\|_{H^{1/2}(\Gamma_i)} + k^{1/2} \|f_i\|_{L^2(\Gamma_i)}\right]$
together with
and $\|L^\pm  (f_1,f_2)\|_{1,k,S}
\leq C \sum_{i=1}^2 k^{-1/2}  \|f_i\|_{L^{2}(\Gamma_i)}$. In particular, the operator
$L^+$ provides (locally) the desired lifting for convex corners.

For the case of a non-convex sector, i.e., $\pi < \omega < 2\pi$, let $%
S^\prime:= B_{2\delta} \setminus S$ and let $E: H^2(S^\prime) \rightarrow
H^2({\mathbb{R}}^2)$ be the extension operator of Stein, \cite[Chap.~VI]%
{stein70}. Then $S^\prime$ is a convex sector of the form considered above.
Then it is easy to check that $(E(L^-(-f_1,-f_2)))|_{S^\prime} \in
H^2(S^\prime)$ has the desired lifting property for $S$. \newline
\emph{step~3:} Localizing with the aid of partitions of unity, we can reduce
the construction of the lifting to the question of liftings from an infinite
line to a half space and from two edges that meet at a common vertex $V$ to
the enclosed sector. The first case can be treated by similar arguments.
The second case is covered by step~2.
\qed
\end{proof}
\begin{proof}
We start from \cite{melenk-sauter09} we states that
on the elements away from the corner, we have the approximation property
$$
\|u - I u\|_{1,k} \leq C k 
\left[ \min\{1,kh\}^{1-\beta_{min}} e^{-bp} + (kh/\sigma_0 p)^p \right]
$$
For the elements abutting $K$ the vertex $A_i$ of the polygon, we have
by Lemma~\ref{lemma:approximation-of-H^22beta-fcts} and a scaling argument
$$
\|u - I u\|_{1,k} \leq C\left[ \left(\frac{h}{p}\right)^{1-\beta_i} + k \left(\frac{h}{p}\right)^{2-\beta_i}
\right] \|r^{\beta_i} \nabla^2 u\|_{L^2(K)}
$$
Hence
\begin{eqnarray*}
\|u - I u\|_{1,k} &\leq & 
C\left[ \left(\frac{h}{p}\right)^{1-\beta_i} + k \left(\frac{h}{p}\right)^{2-\beta_i}
\right] k^{-\beta_i}  \|\Phi_{0,\overrightarrow \beta, k} \nabla^2 u\|_{L^2(K)} \leq 
C\left[ \left(\frac{h}{p}\right)^{1-\beta_i} + k \left(\frac{h}{p}\right)^{2-\beta_i}
\right] k^{2-\beta_i} \\
&\leq& C k \left( 1 + \frac{kh}{p}\right) \left(\frac{hk}{p}\right)^{1-\beta_i}
\end{eqnarray*}
\qed
\end{proof}

