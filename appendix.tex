For the reference triangle $\widehat K:= \{(x,y)\colon 0 < x < 1, 0 < y < 1-x\}$
and $\beta \in [0,1)$ the following two lemmas require the spaces 
$H^{1,1}_{\beta}(\widehat K)$, $H^{2,2}_\beta(\widehat K)$ as well as 
the Besov spaces $B^s_{2,\infty}(\widehat K)$. The spaces $B^s_{2,\infty}(\widehat K)$
are defined by interpolation using the $K$-functional (see, e.g., 
\cite[Chap.~{12}]{brenner-scott94}). For $m \in \{1,2\}$, the spaces 
$H^{m,m}_\beta(\widehat K)$ are determined by the norm 
$
\|u\|^2_{H^{m,m}_\beta(\widehat K)} := \|u\|^2_{H^{m-1}(\widehat K)} + \|r^\beta \nabla^m u\|^2_{L^2(\widehat K)},
$
where $r$ denotes the distance from the origin.
\begin{Lemma}
\label{lemma:besov-space-embedding}
Let $\widehat K = \{(x,y)\colon 0 < x < 1, 0 < y < 1-x\}$ be the 
reference triangle. Let $\beta \in [0,1)$. Then the space 
$H^{2,2}_{\beta}(\widehat K)$ is embedded in the Besov space 
$B^{2-\beta}_{2,\infty}(\widehat K)$ and the space 
$H^{1,1}_{\beta}(\widehat K)$ is embedded in $B^{1-\beta}_{2,\infty}(\widehat K)$. 
In particular, therefore, the embeddings $H^{2,2}_{\beta}(\widehat K) \subset 
H^{2-\beta-\varepsilon}(\widehat K)$ and 
$H^{1,1}_{\beta}(\widehat K) \subset H^{1-\beta-\varepsilon}(\widehat K)$ are compact for 
all $\varepsilon > 0$. 
\end{Lemma}
\begin{proof}
The argument follows ideas presented in \cite[Thm.~{2.1}]{babuska-osborn91}
and \cite{babuska-kellogg79a}. 
We start with the following two Hardy inequalities for sufficiently
smooth functions
\begin{eqnarray}
\label{eq:lemma:besov-space-embedding-1}
\|r^{\beta - 1}\nabla u\|_{L^2(\widehat K)} &\leq& C \|u\|_{H^{2,2}_{\beta}(\widehat K)}\\ 
\label{eq:lemma:besov-space-embedding-2}
\|r^{\beta - 2} (u - u(0))\|_{L^2(\widehat K)} &\leq & C \|u\|_{H^{2,2}_{\beta}(\widehat K)}; 
\end{eqnarray}
here, 
(\ref{eq:lemma:besov-space-embedding-1}) is shown, for example, in \cite[Lemma~{A.1.7}]{melenk02}
and (\ref{eq:lemma:besov-space-embedding-2}) follows from combining \cite[Lemma~{4.2}]{babuska-kellogg79a}
with (\ref{eq:lemma:besov-space-embedding-1}). 
We note also \cite[(2.2)]{babuska-kellogg79a} states the continuous
embedding $H^{2,2}_{\beta}(\widehat K) \subset C(\overline{\widehat K})$ so that $u(0)$ 
in (\ref{eq:lemma:besov-space-embedding-2}) is indeed well-defined. 

We employ the real method of interpolation and write $B^{2-\beta}_{2,\infty} = (L^2,H^2)_{1-\beta/2,\infty}$. 
Our method of proof consists in showing that for $\theta = 1-  \beta/2$ we have 
$$
\sup_{t \in (0,1)} t^{-\theta} K(t,\widetilde u) \leq C \|u\|_{H^{2,2}_{\beta}(\widehat K)}
$$
for some $C > 0$ independent of $u$. To that end, 
we proceed as in the proof of \cite[Lemma~{2.1}]{babuska-kellogg79a}. 
Let for every $\delta > 0$ $\chi_\delta \in C^\infty_0(\BbbR^2)$ with 
$\chi \equiv 1$ on $B_{\delta/2}(0)$ and 
$\operatorname*{supp} \chi_\delta B_\delta(0)$ and 
$\|\nabla^j \chi_\delta\|_{L^\infty(\BbbR^2)} \leq C \delta^{-j}$, $j \in \{0,1,2\}$. We define the splitting 
$$
\widetilde u = \chi_\delta \widetilde u + (1 - \chi_\delta) \widetilde u 
=: u_1 + u_2
$$
Then from (\ref{eq:lemma:besov-space-embedding-1}) and (\ref{eq:lemma:besov-space-embedding-2})
\begin{align*}
&\|\chi_\delta \widetilde u\|_{L^2(\widehat K)}  \leq 
C \|\widetilde u\|_{L^2(\widehat K\cap B_{\delta}(0))} 
\leq \delta^{2-\beta} \|r^{\beta - 2} \widetilde u\|_{L^2(\widehat K)} 
\leq C \delta^{2-\beta} \|u\|_{H^{2,2}_{\beta}(\widehat K)},  \\
& |(1 - \chi_\delta) \widetilde u|_{H^2(\widehat K)} \leq \\
&
C \delta^{-2} \|\widetilde u\|_{L^2(\widehat K\setminus B_{\delta/2}(0))} + 
C \delta^{-1} \|\nabla \widetilde u\|_{L^2((\widehat K \cap B_{\delta}(0) )\setminus B_{\delta/2}(0))} + 
C \|\nabla^2 \widetilde u\|_{L^2((\widehat K \cap B_{\delta}(0) )\setminus B_{\delta/2}(0))} \\
&\leq C \delta^{-2+2-\beta} \|r^{\beta-2}\widetilde u\|_{L^2(\widehat K)}+
C \delta^{-1+1-\beta} \|r^{\beta-1} \nabla \widetilde u\|_{L^2((\widehat K)} + 
C \delta^{-\beta}\|r^\beta \nabla^2 \widetilde u\|_{L^2((\widehat K)} \\
&\leq C \delta^{-\beta} \|u\|_{H^{2,2}_{\beta}(\widehat K)}. 
\end{align*}
From this, we can infer for any $\delta \in (0,1)$
$$
K(t,\widetilde u) \leq \|u_1\|_{L^2(\widehat K)} + t \|u_2\|_{H^2(\widehat K)}
\leq C \|u\|_{H^{2,2}_{\beta}(\widehat K)}\left[ \delta^{2-\beta} + t \delta^{-\beta}\right].
$$
Selecting $\delta = t^{1/2}$ gives 
$K(t,\widetilde u) \leq C t^{1-\beta/2} \|u\|_{H^{2,2}_{\beta}(\widehat K)}$. 
\qed
\end{proof}
%
%
\begin{Lemma}
\label{lemma:trace-inequality-H11beta}
Let $\beta \in [0,1/2)$ and $\widehat K$ be the reference triangle. Then 
there exists $C > 0$ such that for all $u \in H^{1,1}_\beta(\widehat K)$
there holds 
$
\|u\|_{L^2(\partial \widehat K)} \leq C \left[\|u\|_{L^2(\widehat K)} + 
\|r^\beta \nabla u\|_{L^2(\widehat K)}\right] . 
$
\end{Lemma}
\begin{proof}
For $s > 1/2$, we have the 
inequality $\|u\|_{L^2(\partial\widehat K)} \leq C_s \|u\|_{H^s(\widehat K)}$. 
From the embedding $H^{1,1}_\beta(\widehat K) \subset H^{1-\beta}(\widehat K)$
of Lemma~\ref{lemma:besov-space-embedding}, we then get 
$
\|u\|_{L^2(\partial\widehat K)} \leq C\|u\|_{H^s(\widehat K)} 
\leq C \left[ \|u\|_{L^2(\widehat K)} + \|r^\beta \nabla u\|_{L^2(\widehat K)}\right]. 
$
\qed
\end{proof}
%----------------------
\iftechreport
%----------------------
\section*{Further results and proofs} 
%----------------------
\begin{numberedproof}{Remark~\ref{rem:pum-approximation}}
By interpolation using the $K$-functional, every 
$u \in H^{1+\theta}(\Omega)$ with $\theta \in (0,1)$ 
can be decomposed as 
$
u = u_1 + u_2
$
with 
\begin{equation}
\label{eq:rem-pum-approximation-10}
\|u_1\|_{H^1(\Omega)} \leq t^{\theta} \|u\|_{H^{1+\theta}(\Omega)}, 
\qquad 
\|u_2\|_{H^2(\Omega)} \leq t^{\theta-1} \|u\|_{H^{1+\theta}(\Omega)}, 
\end{equation}
where $t > 0$ is arbitrary. The proof of Lemma~\ref{lemma:PUM-approximation}
shows that $u_1$ and $u_2$ can be approximated from $V_N$ as follows: 
\begin{eqnarray*}
\inf_{v \in V_N} \|u_2 - v\|_{L^2(\Omega)} + h \|\nabla (u_2 - v)\|_{L^2(\Omega)} &\leq& 
h^2 \|u_2\|_{H^2(\Omega)} + (kh)^2 \|u_2 \|_{L^2(\Omega)} \\
\inf_{v \in V_N} \|u_1 - v\|_{L^2(\Omega)} + h \|\nabla (u_1 - v)\|_{L^2(\Omega)} &\leq& 
h \|u_1\|_{H^1(\Omega)} + (kh)^2 \|u_1 \|_{L^2(\Omega)}.  
\end{eqnarray*}
Using $t = h$ in (\ref{eq:rem-pum-approximation-10}) we therefore get 
\begin{eqnarray*}
\inf_{v \in V_N} \|u - v\|_{L^2(\Omega)} + h \|\nabla (u_2 - v)\|_{L^2(\Omega)} &\leq& 
h^{1+\theta} \|u\|_{H^{1+\theta}(\Omega)} + (kh)^2 \left[ \|u_1\|_{L^2(\Omega)} + \|u_2 \|_{L^2(\Omega)}\right].
\end{eqnarray*} 
The decomposition $u = u_1 + u_2$ and the 
triangle inequality yield $\|u_1\|_{L^2(\Omega)} + \|u_2\|_{L^2(\Omega)} \leq 
\|u\|_{L^2(\Omega)} + 2 \|u_1\|_{L^2(\Omega)} \leq 
\|u\|_{L^2(\Omega)} + 2 \|u_1\|_{H^1(\Omega)} \leq 
\|u\|_{L^2(\Omega)} + 2 h^{\theta} \|u\|_{H^{1+\theta}(\Omega)}$. Combining these estimates, we obtain  
\begin{eqnarray*}
\inf_{v \in V_N} \|u - v\|_{L^2(\Omega)} + h \|\nabla (u_2 - v)\|_{L^2(\Omega)} &\leq& 
\left(h^{1+\theta} + (kh)^2 h^\theta\right)\|u\|_{H^{1+\theta}(\Omega)} + (kh)^2 \|u\|_{L^2(\Omega)}, 
\end{eqnarray*} 
which concludes the proof. 
\end{numberedproof}

\begin{Lemma}
Let $\beta \in [0,1)$. Then for $0 < r < R$ and all $n \in \BbbN_0$
$$
\min\left( 1, \frac{r}{\min\left\{1, \frac{n+1}{k+1}\right\}}\right)^{n+\beta}
\frac{1}{\max\{n,k\}^{n+2}}
\leq C k^{-(2-\beta)} \gamma^n r^{n+\beta} \frac{1}{n^{n+2}}
$$
\end{Lemma}
\begin{proof}
We denote the left-hand side by $lhs$ and the right-hand by rhs. 
We consider several cases. 

{\em case 1: $n\leq k$ and $r(k+1) \leq n+1$}: 
\begin{eqnarray*}
lhs &=& 
\left(\frac{(k+1)r}{n+1}\right)^{n+\beta}\frac{1}{k^{n+2}}  \\
& = & 
r^{n+\beta} \frac{1}{n^{n+2}} \left(\frac{n}{n+1}\right)^{n+2}(n+1)^{2-\beta}
\left(\frac{k+1}{k}\right)^{n+2} (k+1)^{-(2-\beta)}\\
&\leq& C \gamma^n k^{-(2-\beta)} r^{n+\beta} \frac{1}{n^{n+2}}
\end{eqnarray*}
for suitable $C$, $\gamma > 0$ if we assume that $k \ge k_0 > 0$. 
\newline 
{\em case 2: $n\leq k$ and $r(k+1) >  n+1$}:
\begin{eqnarray*}
rhs &=& \frac{1}{k^{n+2}}  = 
\frac{1}{k^{2-\beta}} \frac{1}{k^{n+\beta}}
= 
\frac{1}{k^{2-\beta}} \left(\frac{k+1}{k}\right)^{n+\beta} \frac{1}{(k+1)^{n+\beta}}\\
&\leq& 
\frac{1}{k^{2-\beta}} \left(\frac{k+1}{k}\right)^{n+\beta} \left(\frac{r}{n+1}\right)^{n+\beta}\\
&\leq & 
C \gamma^n k^{-(2-\beta)} r^{n+\beta} \frac{1}{n^{n+2}}
\end{eqnarray*}
for suitable $C$, $\gamma > 0$. 
\newline 
{\em case 3: $n>k$}: Then, for $0 < r < R$
\begin{eqnarray*} 
lhs &=& \left(\min\{1,r\}\right)^{n+\beta} \frac{1}{n^{n+2}}
=   r^{n+\beta} \frac{1}{n^{n+2}} 
\leq k^{-(2-\beta)} r^{n+\beta} \frac{1}{n^{n+2}} n^{2-\beta}\\
& \leq & 
C k^{-(2-\beta)} r^{n+\beta} \frac{1}{n^{n+2}} \gamma^n 
\end{eqnarray*} 
for suitable $C$, $\gamma > 0$. 
\qed
\end{proof}
\begin{Lemma}
\label{lemma:approximation-of-H^22beta-fcts}
Let $\beta \in [0,1)$. Then, 
for every $p \in \BbbN$ there exists a linear operator 
$\pi_p: H^{2,2}_{\beta}(\widehat K) \rightarrow {\mathcal P}_p$ that admits an ``element-by-element construction
in the sense of \cite[Def.~{5.3}]{melenk-sauter10} with the following approximation property: 
$$
p\|u - \pi_p u\|_{L^2(\widehat K)} + \|u - \pi_p u\|_{H^1(\widehat K)} \leq 
C p^{-(1-\beta)} \|r^\beta \nabla^2 u\|_{L^2(\widehat K)}, 
$$
where $C > 0$ is independent of $p$ and $u$. 
\end{Lemma}
\begin{proof}
Inspection of the proof of \cite[Thm.~B.4]{melenk-sauter10} shows that the operator 
$\pi_p$ constructed there does in fact not depend on the regularity parameter $s > 1$. It has (as stated
in \cite[Thm.~{B.4}]{melenk-sauter10}), the approximation property  
\begin{equation}
\label{eq:lemma:approximation-of-H^22beta-fcts-1}
p\|u - \pi_p u\|_{L^2(\widehat K)} + \|u - \pi_p u\|_{H^1(\widehat K)} \leq 
C p^{-(s-1)} |u|_{H^s(\widehat K)} \qquad \forall u \in H^s(\widehat K), 
\end{equation}
if $p \ge s-1$. 
Upon writing the Besov space $B^{s}_{2,\infty}$ as an interpolation space  
$B^{s}_{2,\infty} = (H^2(\widehat K),H^{1}(\widehat K))_{s-1,\infty}$ for $s \in (1,2)$, 
we can infer for $s \in (1,2)$ from (\ref{eq:lemma:approximation-of-H^22beta-fcts-1}) 
the slightly stronger statement 
\begin{equation}
\label{eq:lemma:approximation-of-H^22beta-fcts-2}
p\|u - \pi_p u\|_{L^2(\widehat K)} + \|u - \pi_p u\|_{H^1(\widehat K)} \leq 
C p^{-(s-1)} \|u\|_{B^s_{2,\infty}(\widehat K)} \qquad \forall u \in B^s_{2,\infty}(\widehat K). 
\end{equation}
Appealing to Lemma~\ref{lemma:besov-space-embedding} then yields 
\begin{equation}
\label{eq:lemma:approximation-of-H^22beta-fcts-3}
p\|u - \pi_p u\|_{L^2(\widehat K)} + \|u - \pi_p u\|_{H^1(\widehat K)} \leq 
C p^{-(s-1)} \|u\|_{H^{2,2}_{\beta}(\widehat K)}.  
\end{equation}
We replace the full $H^{2,2}_{\beta}(\widehat K)$ norm by the seminorm in the standard
way by a compactness argument. Since $H^{2,2}_\beta(\widehat K)$ is compactly
embedded in $H^1(\widehat K)$ (see, e.g., \cite[Lemma~{4.19}]{schwab98}) one obtains 
$\inf_{v \in {\mathcal P}_1} \|u - v\|_{H^{2,2}_{\beta}(\widehat K)} \leq 
C \|r^\beta \nabla^2 u\|_{L^2(\widehat K)}$. The proof is completed by noting that 
(\ref{eq:lemma:approximation-of-H^22beta-fcts-1}) implies that 
$\pi_p$ reproduces linear polynomials.  
\qed
\end{proof}
%----------------------
\begin{numberedproof}{Lemma~\ref{lemma:1d-optimal}}
One has the {\sl a priori} bound
\begin{equation}
\label{eq:lemma:1d-optimal-10}
\|u\|_{1,k} \leq C \|f\|_{L^2(\Omega)}
\end{equation}
for the solution $u$ of (\ref{eq:model-problem-1d})
(as for the model problem (\ref{eq:problem}), this can be shown
using the test function $v = x u^\prime$; an alternative proof
based on the Green's function is given in
\cite[Thm.~{4.4}]{ihlenburg98}). If one denotes  by $\varphi_i$ the
classical piecewise linear hat function associated with node $x_i$,
then one has by Taylor expansion
\begin{equation}
\label{eq:lemma:1d-optimal-20}
\|\varphi_i - \psi_i\|_{L^\infty(K)} 
\leq C ( k h_K)^2, 
\qquad 
\|(\varphi_i - \psi_i)^\prime\|_{L^\infty(K)} 
\leq C  k^2 h_K,
\qquad \forall K \in {\mathcal T}. 
\end{equation}
The approximation properties follow easily from the nodal exactness.
Specifically, denoting by $I u$ the classical piecewise linear interpolant
of $u$ and by $\widetilde I u \in V_N^{opt}$ the nodal interpolant
determined by $\widetilde I u(x_i) = u(x_i)$ for all $i \in \{0,\ldots,N\}$,
we have the well-known estimate
$$
\|u - I u\|_{1,k} \leq C (kh^2 + h)\|u^{\prime\prime}\|_{L^2(\Omega)}
\leq C kh ( 1 + kh)\|f\|_{L^2(\Omega)}, 
$$
where we used the differential equation and
the bound (\ref{eq:lemma:1d-optimal-10})
to estimate $\|u^{\prime\prime}\|_{L^2(\Omega)} 
\leq \|f\|_{L^2(\Omega)} + k^2 \|u\|_{L^2(\Omega)} 
\leq C k \|f\|_{L^2(\Omega)}$. Next, we estimate the difference
$Iu - \widetilde Iu$. The multiplicative trace inequality takes the form
\begin{equation}
\label{eq:lemma:1d-optimal-30}
h_K \|w\|^2_{L^\infty(K)} \leq C \left[ \|w\|^2_{L^2(K)} + h_K \|w\|_{L^2(K)} \|w^\prime\|_{L^2(K)}
\right] \qquad \forall w \in H^1(K). 
\end{equation}
Hence, the estimates (\ref{eq:lemma:1d-optimal-20}), (\ref{eq:lemma:1d-optimal-30})
imply
\begin{eqnarray*}
\|Iu - \widetilde I u\|^2_{1,k} &\leq& 
C \sum_{K \in {\mathcal T}} k^2 (kh_K)^2 (1 + (kh_K)^2) \|u\|^2_{L^\infty(K)} \\
& \leq & C (kh)^2 (1 + (kh)^2) 
\left[ 
k^2 \|u\|^2_{L^2(\Omega)} + k^2 h^2 \|u^\prime\|^2_{L^2(\Omega)}
\right]
\\
& \leq &
C (kh)^2 (1 + (kh)^2)^2 \|u\|^2_{1,k}. 
\end{eqnarray*}
An appeal to (\ref{eq:lemma:1d-optimal-10}) concludes the argument.
\end{numberedproof}
%----------------------
\else
%----------------------
\fi %end iftechreport
%----------------------
