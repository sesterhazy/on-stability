clear all; 
nmax = 17; ;
k_list = [10 100 1000]; 
p_list  = [1 2 3 4]; 
mmax_list = [17 16 12 12]; 
  k = k_list(1)
  dof1 = zeros(length(p_list),mmax_list(1)); 
  errh1_1 = zeros(length(p_list),mmax_list(1)); 
  errl2_1 = zeros(length(p_list),mmax_list(1)); 
  err_1 = zeros(length(p_list),mmax_list(1)); 
for i=1:length(p_list)
  z = [1:mmax_list(i)]; 
  [dof1(i,z),errh1_1(i,z),errl2_1(i,z),err_1(i,z)]=...
     hpFEM_1D(mmax_list(i),p_list(i),k); 
   ppw1(i,:) = dof1(i,:)./(k/(2*pi));  
end
  k = k_list(2)
  dof2 = zeros(length(p_list),mmax_list(1)); 
  errh1_2 = zeros(length(p_list),mmax_list(1)); 
  errl2_2 = zeros(length(p_list),mmax_list(1)); 
  err_2 = zeros(length(p_list),mmax_list(1)); 
for i=1:length(p_list)
  z = [1:mmax_list(i)]; 
  [dof2(i,z),errh1_2(i,z),errl2_2(i,z),err_2(i,z)]=...
     hpFEM_1D(mmax_list(i),p_list(i),k); 
   ppw2(i,:) = dof2(i,:)./(k/(2*pi));  
end
  k = k_list(3)
  dof3 = zeros(length(p_list),mmax_list(1)); 
  errh1_3 = zeros(length(p_list),mmax_list(1)); 
  errl2_3 = zeros(length(p_list),mmax_list(1)); 
  err_3 = zeros(length(p_list),mmax_list(1)); 
for i=1:length(p_list)
  z = [1:mmax_list(i)]; 
  [dof3(i,z),errh1_3(i,z),errl2_3(i,z),err_3(i,z)]=...
     hpFEM_1D(mmax_list(i),p_list(i),k); 
   ppw3(i,:) = dof3(i,:)./(k/(2*pi));  
end

figure(1)
loglog(ppw1(1,:),errh1_1(1,:),...
       ppw2(1,:),errh1_2(1,:),...
       ppw3(1,:),errh1_3(1,:),...
       'LineWidth',2)
set(gca,'FontSize',18)
title('p=1')
legend('k=10','k=100','k=1000')
xlabel('DOF per wave length')
%axis([0.1 100 10^(-3) 10])

figure(2)
loglog(ppw1(2,:),errh1_1(2,:),...
       ppw2(2,:),errh1_2(2,:),...
       ppw3(2,:),errh1_3(2,:),...
       'LineWidth',2)
set(gca,'FontSize',18)
title('p=2')
legend('k=10','k=100','k=1000')
xlabel('DOF per wave length')
%axis([0.1 100 10^(-3) 10])

figure(3)
loglog(ppw1(3,:),errh1_1(3,:),...
       ppw2(3,:),errh1_2(3,:),...
       ppw3(3,:),errh1_3(3,:),...
       'LineWidth',2)
set(gca,'FontSize',18)
title('p=3')
legend('k=10','k=100','k=1000')
xlabel('DOF per wave length')
%axis([0.1 100 10^(-3) 10])

figure(4)
loglog(ppw1(4,:),errh1_1(4,:),...
       ppw2(4,:),errh1_2(4,:),...
       ppw3(4,:),errh1_3(4,:),...
       'LineWidth',2)
set(gca,'FontSize',18)
title('p=4')
legend('k=10','k=100','k=1000')
xlabel('DOF per wave length')
%axis([0.1 100 10^(-3) 10])

save 'results.mat' k_list p_list mmax_list dof1 errh1_1 errl2_1 err_1 ppw1 dof2 errh1_2 errl2_2 err_2 ppw2 dof3 errh1_3 errl2_3 ppw3  
