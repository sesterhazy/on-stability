function [DOF,errh1,errl2,err]= hpFEM_1D(mmax,p,k)
%
%Beispiel einer Implementierung von hp-FEM in 1D
%
 cg = -(0.2e1 * sin(k) - 0.3e1 * k - sin(k) * cos(k) + 0.2e1 * k * cos(k))  / (k^3*0.2e1);
 h1 = sqrt(cg) 
 cg0 = -(0.6e1 * sin(k) - 0.5e1 * k - 0.3e1 * sin(k) * cos(k) + 0.2e1 * k * cos(k))/(k^5*0.2e1);
 l2 = sqrt(cg0); 
  errh1 = zeros(1,mmax); 
  errl2 = zeros(1,mmax); 
  err = zeros(1,mmax); 
  DOF = zeros(1,mmax); 

  for m=1:mmax
    m
    N(m) = 2^m; 
    [x,u] = fem_1D(N(m),p,k); 
    %[x1,u1] = compute_L2_projection(N(m),p,k,@u_exact)
    errh1(m) = compute_H1_error(x,u,p,@uprime_exact,k)/(h1); 
    errl2(m) = compute_L2_error(x,u,p,@u_exact,k)/l2; 
    err(m) = sqrt( ( k^2*(errl2(m)*l2)^2+ (errh1(m)*h1)^2 )/(k^2*l2*l2+h1*h1)); 

    DOF(m) = length(u); 
  end   
return
end
%-----------------------------------
function [x,u]=solve(k,N);
  %code for p=1
g = 0;
h = 1.0/(N-1);
e = ones(N,1);
A = spdiags([-e 2*e -e], -1:1, N, N); %-u'' part
A(1,1) = 1;
A(N,N) = 1;
M = spdiags([1/6*e 2/3*e 1/6*e],-1:1,N,N);
M(1,1) = 1/3;
M(N,N) = 1/3;
K = (1/h)*A-(h*k*k)*M;
K(N,N) = K(N,N)-i*k; %boundary condition at right endpoint
b = h*ones(N,1);
b(1) = b(1)/2;
b(N) = b(N)/2;
b(N) = b(N)+g;
u = K(2:N,2:N)\b(2:N);
u = [0; u];
x = h*[0:N-1];
return
end
%-----------------------------
function u = exact_sol(k,N);
%solves with f = 1, g = 0
h = 1/(N-1);
b = 1/k^2;
a = ( (b*k*sin(k)+i*b*k*cos(k)-i/k) )/(k*cos(k)-i*k*sin(k));
x = h*[0:N-1]';
u = -1/k^2 + a*sin(k*x) + b*cos(k*x);
return
end
%-----------------------------
function y = u_exact(k,x);
b = 1/k^2;
a = ( (b*k*sin(k)+i*b*k*cos(k)-i/k) )/(k*cos(k)-i*k*sin(k));
y = -1/k^2 + a*sin(k*x) + b*cos(k*x);
return
end
%-----------------------------
function y = uprime_exact(k,x);
b = 1/k^2;
a = ( (b*k*sin(k)+i*b*k*cos(k)-i/k) )/(k*cos(k)-i*k*sin(k));
y =  a*k*cos(k*x) - b*k*sin(k*x);
return
end
%-------------------------------------------------------------------------------
function [mesh,u] = fem_1D(m,p,k)
%m = parameter for the mesh (here: uniform mesh)
%p = polynomial degree 
%
%numbering of the unknowns is such that the first and the 
%last unknown correspond to the left and right endpts 
%

g = 0;                   %value of the Neumann bc at the right endpt 

%define the mesh
xleft = 0; xright = 1; 
h = (xright-xleft)/m; 
mesh =[xleft:h:xright];   %uniform mesh

%compute the stiffness matrix and the load vector without boundary conditions 
%for the model problem - u^{\prime\prime} -k^2 u  = f
[K,M,l] = assemble_neumann_problem(mesh,p); 
B = K-k*k*M; 
N = size(B,1);            % get size of B
l(N) = l(N)+g;            % add the  Neumann boundary conditions 
                          % at the right endpoint
B(N,N) = B(N,N) - i*k;    % contribution for Robin b.c.

BD = B([2:N],[2:N]);      %drop the first row and column to enforce 
                          %homogeneous Dirichlet b.c. at left end point
lD = l([2:N]);            %also drop the first entry of load vector
u = BD\lD;                %solve 
u = [0;u];                %fit in the missing value at the left endpoint 
return
end
%-------------------------------------------------------------------------------
function [mesh,u] = compute_L2_projection(m,p,k,fct)
%m = parameter for the mesh (here: uniform mesh)
%p = polynomial degree 
%
%numbering of the unknowns is such that the first and the 
%last unknown correspond to the left and right endpts 
%

g = 0;                   %value of the Neumann bc at the right endpt 

%define the mesh
xleft = 0; xright = 1; 
h = (xright-xleft)/m; 
mesh =[xleft:h:xright];   %uniform mesh

%compute the stiffness matrix and the load vector without boundary conditions 
%for the model problem - u^{\prime\prime} -k^2 u  = f
[M,l] = assemble_mass_matrix(mesh,p,k,fct); 
N = size(M,1);            % get size of B
u = M\l;                %solve 
return
end
%-------------------------------------------------------------------------------
function err = compute_L2_error_FE_fcts(mesh,u,p,u1,k); 
%input: a mesh, i.e., a (sorted) list of nodes x1, x2,...,xM
%input: solution vector u (including the Dirichlet value)
%input: second vector u1 (including the Dirichlet value) that represents another FE-fct
%input: polynomial degree p
%input: function handle to exact solution 
%output: L^2-norm of error 
%
%
q = p+2;           %number of Gaussian quadrature points to be employed 
[x,w] = gauleg(q); %x = quadrature points on (-1,1); w = quadrature weights
x = x'; w = w';    %gauleg returns column vectors, we want row vectors 
PHI = N(x,p+1);                     %evaluate the shape fcts N_i at the quadrature points 
[T,ndof] = local_to_global_maps(mesh,p); 

err = 0; 
for K=1:length(mesh)-1 
  h = mesh(K+1) - mesh(K); 
  for j=1:q
    ufe = 0; 
    for m=1:p+1
      ufe = ufe + (u(T(K,m))-u1(T(K,m)))*PHI(m,j); 
    end 
    err = err + h/2*w(j)*(abs(ufe))^2; 
  end
end 
err = sqrt(err); 
return
end
%-------------------------------------------------------------------------------
function err = compute_L2_error(mesh,u,p,uexact,k); 
%input: a mesh, i.e., a (sorted) list of nodes x1, x2,...,xM
%input: solution vector u (including the Dirichlet value)
%input: polynomial degree p
%input: function handle to exact solution 
%output: L^2-norm of error 
%
%
q = p+2;           %number of Gaussian quadrature points to be employed 
[x,w] = gauleg(q); %x = quadrature points on (-1,1); w = quadrature weights
x = x'; w = w';    %gauleg returns column vectors, we want row vectors 
PHI = N(x,p+1);                     %evaluate the shape fcts N_i at the quadrature points 
[T,ndof] = local_to_global_maps(mesh,p); 

err = 0; 
for K=1:length(mesh)-1 
  h = mesh(K+1) - mesh(K); 
  F = feval(uexact,k,(h/2*(x+1)+mesh(K)));  %evaluate uexact at quadrature points 
  for j=1:q
    ufe = 0; 
    for m=1:p+1
      ufe = ufe + u(T(K,m))*PHI(m,j); 
    end 
    err = err + h/2*w(j)*(abs((F(j)-ufe)))^2; 
  end
end 
err = sqrt(err); 
return
end
%-------------------------------------------------------------------------------
function err = compute_H1_error(mesh,u,p,uexact,k); 
%input: a mesh, i.e., a (sorted) list of nodes x1, x2,...,xM
%input: solution vector u (including the Dirichlet value)
%input: polynomial degree p
%input: function handle to exact solution 
%output: H^1-seminorm error 
%
%
q = p+2;           %number of Gaussian quadrature points to be employed 
[x,w] = gauleg(q); %x = quadrature points on (-1,1); w = quadrature weights
x = x'; w = w';    %gauleg returns column vectors, we want row vectors 
GRAD_PHI = grad_N(x,p+1); %evaluate N_i' at the quadrature points
[T,ndof] = local_to_global_maps(mesh,p); 

err = 0; 
for K=1:length(mesh)-1 
  h = mesh(K+1) - mesh(K); 
  F = feval(uexact,k,(h/2*(x+1)+mesh(K)));  %evaluate uexact at quadrature points 
  for j=1:q
    ufe = 0; 
    for m=1:p+1
      ufe = ufe + u(T(K,m))*GRAD_PHI(m,j)*2/h; 
    end 
    err = err + h/2*w(j)*(abs((F(j)-ufe)))^2; 
  end
end 
err = sqrt(err); 
return
end
%-------------------------------------------------------------------------------
function [M,l] = assemble_mass_matrix(mesh,p,k,fct); 
%input: a mesh, i.e., a (sorted) list of nodes x1, x2,...,xM
%input: polynomial degree p
%output: stiffness matrix S and mass matrix M and load vector l
%
%
[T,N] = local_to_global_maps(mesh,p); 
%
M = sparse(N,N); 
l = zeros(N,1); 
for K = 1:length(mesh)-1
 xleft = mesh(K); xright = mesh(K+1); 
 vertex_coords = [xleft; xright]; 
 [BK,MK] = element_stiffness_matrix(vertex_coords,p); 
 lK = element_l2_projection(vertex_coords,p,k,fct); 
 for i=1:p+1
  for j=1:p+1
    M(T(K,i),T(K,j)) = M(T(K,i),T(K,j))+MK(i,j); 
  end;
  l(T(K,i)) = l(T(K,i))+lK(i); 
 end;
end;  
return
end
%-------------------------------------------------------------------------------
function [B,M,l] = assemble_neumann_problem(mesh,p); 
%input: a mesh, i.e., a (sorted) list of nodes x1, x2,...,xM
%input: polynomial degree p
%output: stiffness matrix S and mass matrix M and load vector l
%
%
[T,N] = local_to_global_maps(mesh,p); 
%
B = sparse(N,N); 
M = sparse(N,N); 
l = zeros(N,1); 
for K = 1:length(mesh)-1
 xleft = mesh(K); xright = mesh(K+1); 
 vertex_coords = [xleft; xright]; 
 [BK,MK] = element_stiffness_matrix(vertex_coords,p); 
 lK = element_load_vector(vertex_coords,p); 
 for i=1:p+1
  for j=1:p+1
    B(T(K,i),T(K,j)) = B(T(K,i),T(K,j))+BK(i,j); 
    M(T(K,i),T(K,j)) = M(T(K,i),T(K,j))+MK(i,j); 
  end;
  l(T(K,i)) = l(T(K,i))+lK(i); 
 end;
end;  
return
end
%-------------------------------------------------------------------------------
function [T,N] = local_to_global_maps(mesh,p) 
%input: the mesh and the polynomial degree 
%output: matrix T, where T(K,i) gives the number of the global DOF 
%        corresponding to the function N_i on element K
%        N = total number of DOF 
%        recall: N_1, N_2 are linears, N_i for i \ge 3 are bubbles 
%        global numbering: for each element, first the left endpt, 
%                          then all bubbles, then the right endpt 
%
T = zeros(length(mesh)-1,p+1); 
N = 1; 
for K=1:length(mesh)-1 
  T(K,1) = N; 
  N = N +1; 
  for i=3:p+1
    T(K,i) = N ; 
    N = N +1; 
  end; 
  T(K,2) = N ; 
end;
return
end
%-------------------------------------------------------------------------------
function [K,M] = element_stiffness_matrix(vertex_coords,p) 
% vertex_coords(1:2) contains the two endpoints of the element
% p >= 1 polynomial degree to be employed 
%
h = vertex_coords(2) - vertex_coords(1); 
K = zeros(p+1,p+1); 
M = zeros(p+1,p+1); 
q = p+1;           %number of Gaussian quadrature points to be employed 
[x,w] = gauleg(q); %x = quadrature points on (-1,1); w = quadrature weights
x = x'; w = w';    %gauleg returns column vectors, we want row vectors 
PHI = N(x,p+1);           %evaluate the shape fcts N_i at the quadrature points 
GRAD_PHI = grad_N(x,p+1); %evaluate N_i' at the quadrature points 


   K = GRAD_PHI*diag(w)*GRAD_PHI'/(h/2); %realizes the numerical integration of 
                                         %\int_{-1}^1 N_i^\prime N_j^\prime/(h/2)
   M = PHI*diag(w)*PHI'*h/2;             %numerical integration of 
                                         %\int_{-1}^1 N_i N_j*(h/2)
%
%---comment
% the quadratures to compute K,M are written very compactly. multiplying out, 
% they amount to  setting  M(i,j) = 
% \sum_{k=1}^{length(x)} w(k) PHI(i,k)PHI(j,k)*h/2 for all i,j =1,\ldots,p+1
% similarly for the matrix K
%---endcomment
return
end
%-------------------------------------------------------------------------------
function l = element_load_vector(vertex_coords,p) 
% vertex_coords(1:2) contains the two endpoints of the element
% p >= 1 polynomial degree to be employed 
%
h = vertex_coords(2) - vertex_coords(1); 
l = zeros(p+1,1); 
q = p+1;           %number of Gaussian quadrature points to be employed 
[x,w] = gauleg(q); %x = quadrature points on (-1,1); w = quadrature weights
x = x'; w = w';    %gauleg returns column vectors, we want row vectors 
PHI = N(x,p+1);                     %evaluate the shape fcts N_i at the quadrature points 
F = f(h/2*(x+1)+vertex_coords(1));  %evaluate the right-hand side at the quadrature points 

l = PHI*diag(w)*F'*h/2;             %\int_{-1}^1 f N_i*h/2
%
%---comment
% the quadrature to compute l,M is written very compactly. Multiplying out, the definition of l 
% amounts to  setting l(i) = \sum_{k=1}^{length(x)} w(k) PHI(i,k) * F(k) *h/2    for all i
% similarly for the matrix K
%---endcomment
return
end
%-------------------------------------------------------------------------------
function l = element_l2_projection(vertex_coords,p,k,fct) 
% vertex_coords(1:2) contains the two endpoints of the element
% p >= 1 polynomial degree to be employed 
%
h = vertex_coords(2) - vertex_coords(1); 
l = zeros(p+1,1); 
q = p+1;           %number of Gaussian quadrature points to be employed 
[x,w] = gauleg(q); %x = quadrature points on (-1,1); w = quadrature weights
x = x'; w = w';    %gauleg returns column vectors, we want row vectors 
PHI = N(x,p+1);                     %evaluate the shape fcts N_i at the quadrature points 
F = feval(fct,k,h/2*(x+1)+vertex_coords(1));  %evaluate the right-hand side at the quadrature points 

l = PHI*diag(w)*F'*h/2;             %\int_{-1}^1 f N_i*h/2
%
%---comment
% the quadrature to compute l,M is written very compactly. Multiplying out, the definition of l 
% amounts to  setting l(i) = \sum_{k=1}^{length(x)} w(k) PHI(i,k) * F(k) *h/2    for all i
% similarly for the matrix K
%---endcomment
return
end
%-------------------------------------------------------------------------------
function y = f(x)
%right-hand side function f 
  y = ones(size(x)); 
return
end
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
function [U] =N(x,n)

%
%  Calculates the first n trial functions on [-1,1]; 
%  we assume implicitly that the vector x is a row vector
%
%  N_1 = (1-x)/2
%  N_2 = (1+x)/2
%  N_n = \sqrt{(2n-3)/2}\int_{-1}^x L_{n-2}(t) dt     if n \ge 3
%
  U(1,:) = (1-x)/2;
  U(2,:) = (1+x)/2;
if n >2 
[P] = legtable(x,n);
  for i = 3:n
      ip=i-2;
        U(i,:) = sqrt((2*i-3)/2)*1/(2*ip+1)*(P(ip+2,:)-P(ip,:));
  end;
end
return
end
%-------------------------------------------------------------------------------
function [U] =grad_N(x,n)

%
%  Calculates the derivatives of the first n trial functions on [-1,1]; 
%  we assume implicitly that the vector x is a row vector
%
%  N_1 = (1-x)/2
%  N_2 = (1+x)/2
%  N_n = \sqrt{(2n-3)/2}\int_{-1}^x L_{n-2}(t) dt     if n \ge 3
%
  U(1,:) = -1/2*ones(size(x));
  U(2,:) =  1/2*ones(size(x));
if n >2 
[P] = legtable(x,n);
  for i = 3:n
      ip=i-2;
        U(i,:) = sqrt((2*i-3)/2)*P(ip+1,:);
  end;
end
return
end
%-------------------------------------------------------------------------------
function [P] = legtable(x,m)
%
% input:  row vector x \subset (-1,1) of points
%         m the maximal order of the Legendre polynomials
% output: a matrix P=P(m+1,length(x)) containing the values of the
%         Legendre polynomials at the points x
%
  l=length(x);
  P=ones(m+1,l);
  P(2,:)=x;
  for i=2:m
    P(i+1,:)=((2*i-1)*x.*P(i,:)-(i-1)*P(i-1,:))/i;
  end;
return
end
%-------------------------------------------------------------------------------
function [x,w]=gauleg(n)

x=zeros(n,1);
w=zeros(n,1);
  m=(n+1)/2;
  xm=0.0;
  xl=1.0;
  for i=1:m
    z=cos(pi*(i-0.25)/(n+0.5));
    while 1
      p1=1.0;
      p2=0.0;
      for j=1:n
        p3=p2;
        p2=p1;
        p1=((2.0*j-1.0)*z*p2-(j-1.0)*p3)/j;
      end
      pp=n*(z*p1-p2)/(z*z-1.0);
      z1=z;
      z=z1-p1/pp;
      if (abs(z-z1)<eps), break, end
    end
    x(i)=xm-xl*z;
    x(n+1-i)=xm+xl*z;
    w(i)=2.0*xl/((1.0-z*z)*pp*pp);
    w(n+1-i)=w(i);
  end
return
end
