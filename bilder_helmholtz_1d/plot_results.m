clear all; 
load 'results.mat'; 
figure(1)
loglog(ppw1(1,:),errh1_1(1,:),'*-',...
       ppw2(1,:),errh1_2(1,:),'+--',...
       ppw3(1,:),errh1_3(1,:),'o-.',...
       'LineWidth',3)
set(gca,'FontSize',20)
title('p=1; -u^{\prime\prime}-  k^2 u = 1')
legend('k=10','k=100','k=1000')
xlabel('DOF per wave length')
ylabel('rel. H^1 seminorm error')
set(gca,'YTick',[10^(-3) 10^(-2) 10^(-1) 10^0 10^1])
axis([1 1000 10^(-3) 10])
print -depsc2 p1.eps 

figure(2)
loglog(ppw1(2,:),errh1_1(2,:),'*-',...
       ppw2(2,:),errh1_2(2,:),'+--',...
       ppw3(2,:),errh1_3(2,:),'o-.',...
       'LineWidth',3)
set(gca,'FontSize',20)
title('p=2; -u^{\prime\prime}-  k^2 u = 1')
legend('k=10','k=100','k=1000')
xlabel('DOF per wave length')
ylabel('rel. H^1 seminorm error')
axis([1 100 10^(-4) 10])
set(gca,'YTick',[10^(-4) 10^(-3) 10^(-2) 10^(-1) 10^0])
print -depsc2 p2.eps

figure(3)
loglog(ppw1(3,:),errh1_1(3,:),'*-',...
       ppw2(3,:),errh1_2(3,:),'*--',...
       ppw3(3,:),errh1_3(3,:),'o-',...
       'LineWidth',3)
set(gca,'FontSize',20)
title('p=3; -u^{\prime\prime}-  k^2 u = 1')
legend('k=10','k=100','k=1000')
xlabel('DOF per wave length')
ylabel('rel. H^1 seminorm error')
axis([1 100 10^(-4) 10])
set(gca,'YTick',[10^(-4) 10^(-3) 10^(-2) 10^(-1) 10^0])
print -depsc2 p3.eps

figure(4)
loglog(ppw1(4,:),errh1_1(4,:),'*-',...
       ppw2(4,:),errh1_2(4,:),'+--',...
       ppw3(4,:),errh1_3(4,:),'o-.',...
       'LineWidth',3)
set(gca,'FontSize',20)
title('p=4; -u^{\prime\prime}-  k^2 u = 1')
legend('k=10','k=100','k=1000')
xlabel('DOF per wave length')
ylabel('rel. H^1 seminorm error')
axis([1 100 10^(-4) 10])
set(gca,'YTick',[10^(-4) 10^(-3) 10^(-2) 10^(-1) 10^0])
print -depsc2 p4.eps


